//import SecondClass;

public class MethodsTest
{
	public static void main(String[] args)
	{
		int x = 10;
		/*
		methodNoInputNoReturn();
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(1, 3.5);
		x = methodNoInputReturnInt();
		System.out.println(x);		
		System.out.println(sumSquareRoot(6, 3));
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		*/
		SecondClass s = new SecondClass();
		System.out.println(s.addOne(50));
		System.out.println(s.addTwo(50));
	}
	
	public static void methodNoInputNoReturn()
	{
		int x = 50;
		System.out.println("I'm in a method that takes no input and returns nothing");
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int number)
	{
		System.out.println("Inside the method one input no return");
		System.out.println(number);
	}
	
	public static void methodTwoInputNoReturn(int number, double decimal)
	{
		System.out.println(number);
		System.out.println(decimal);
	}
	
	public static int methodNoInputReturnInt()
	{
		return 6;
	}
	
	public static double sumSquareRoot(int x, int y)
	{
		return Math.sqrt(x+y);
	}
}