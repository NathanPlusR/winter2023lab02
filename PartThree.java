import java.util.Scanner;

public class PartThree
{	
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		AreaComputations ac = new AreaComputations();
		
		System.out.println("Please enter a side of a square ");
		double squareSide = scan.nextDouble();
		
		System.out.println("Please enter the length of your rectangle ");
		double rectangleLength = scan.nextDouble();
		
		System.out.println("Please enter the width of your rectangle ");
		double rectangleWidth = scan.nextDouble();
		
		System.out.println("Your square's area is " + ac.areaSquare(squareSide));
		System.out.println("Your rectangle's area is " + ac.areaRectangle(rectangleLength, rectangleWidth));
	}
}